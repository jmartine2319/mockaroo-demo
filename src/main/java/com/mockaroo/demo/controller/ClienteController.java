package com.mockaroo.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mockaroo.demo.feign.ClienteFeign;
import com.mockaroo.demo.modelo.Clientes;

@RestController
public class ClienteController {
	
	@Autowired
	private ClienteFeign clienteFeign;
	
	@GetMapping("consultarCliente")
	public Clientes consultarCliente(@RequestParam Integer id) {
		List<Clientes> listaClientes = clienteFeign.getAll();
		/*for(Clientes cliente:listaClientes) {
			if(cliente.getIdCliente().equals(id)) {
				return cliente;
			}
		}*/
		//return null;
		Clientes cliente =listaClientes.stream().filter(cli -> cli.getIdCliente().equals(id)).findAny().get();
		return cliente;
	}
}
