package com.mockaroo.demo.feign;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mockaroo.demo.modelo.Clientes;


@FeignClient(name="clientes", url="https://my.api.mockaroo.com")
public interface ClienteFeign {
	@RequestMapping(method = RequestMethod.GET, value = "/clientes.json?key=95d3d4e0")
    List<Clientes> getAll();
}
